﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Newtonsoft.Json;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace Scripts.Service.Authentication.Providers.Impl
{
  public class FacebookAuthService : IAuthenticationProvider
  {
    [JsonIgnore]
    [Inject(ContextKeys.CONTEXT_VIEW)]
    public GameObject contextView { get; set; }

    [JsonIgnore]
    [Inject(ContextKeys.CONTEXT_DISPATCHER)]
    public IEventDispatcher dispatcher { get; set; }

    [JsonIgnore]
    public IEventDispatcher Dispatcher
    {
      get { return dispatcher; }
      set { dispatcher = value; }
    }

    private string _username;

    private string _email;

    private string _userId;

    private string _accessToken;

    private string _photoUrl = string.Empty;

    public string ErrorMessage { get; private set; }

    private bool _waitingForInit;

    private List<string> _permissions;

    public void Init()
    {
      _permissions =
        new List<string>() {"public_profile", "user_friends"};

      if (FB.IsInitialized)
        return;

      FB.Init(OnInitComplete);
    }

    private void OnInitComplete()
    {
      if (_waitingForInit)
      {
        Login();
        _waitingForInit = false;
        return;
      }

      if (FB.IsLoggedIn)
      {
        Debug.Log("fb already logged in");
        FB.API("/me?fields=name,email", HttpMethod.GET, OnUserData);
      }
      else
      {
//        dispatcher.Dispatch(AuthenticationProviderEvent.Inited, ServiceType.Facebook);
      }
    }

    public void Login()
    {
      if (Connected)
      {
        FB.API("/me?fields=name,email", HttpMethod.GET, OnUserData);
        return;
      }

      if (!FB.IsInitialized)
      {
        _waitingForInit = true;
        Debug.Log("fb not initialized");
        FB.Init(OnInitComplete);
        return;
      }

      if (FB.IsLoggedIn)
      {
        Debug.Log("fb already logged in");
        FB.API("/me?fields=name,email", HttpMethod.GET, OnUserData);
        return;
      }

      FB.LogInWithReadPermissions(_permissions, OnLoggedIn);
    }

    private void OnLoggedIn(ILoginResult result)
    {
      if (result.Error != null)
      {
        ErrorMessage = result.Error;
        Debug.Log("Facebook login error : " + ErrorMessage);
//        dispatcher.Dispatch(AuthenticationProviderEvent.LoginError, ServiceType.Facebook);
        return;
      }

      if (result.Cancelled)
      {
//        dispatcher.Dispatch(AuthenticationProviderEvent.Cancelled, ServiceType.Facebook);
        return;
      }


      FB.API("/me?fields=name,email", HttpMethod.GET, OnUserData);
    }

    private void OnUserData(IGraphResult result)
    {
      if (result.Error != null)
      {
        ErrorMessage = result.Error;
//        dispatcher.Dispatch(AuthenticationProviderEvent.LoginError, ServiceType.Facebook);
        return;
      }

      _username = result.ResultDictionary["name"].ToString();
      _userId = result.ResultDictionary["id"].ToString();
      if (result.ResultDictionary.ContainsKey("email"))
        _email = result.ResultDictionary["email"].ToString();
      _accessToken = Facebook.Unity.AccessToken.CurrentAccessToken.TokenString;
      _photoUrl = "/me/picture?redirect=false&type=large&width=128&height=128";
      FB.API(_photoUrl, HttpMethod.GET, ProfilePhotoCallback);
    }

    public bool Connected
    {
      get { return FB.IsLoggedIn; }
    }

    public string Username
    {
      get { return _username; }
    }

    public string UserId
    {
      get { return _userId; }
    }


    public string AccessToken
    {
      get { return _accessToken; }
    }

    public string PhotoUrl
    {
      get { return _photoUrl; }
      set { _photoUrl = value; }
    }

    public string Email
    {
      get { return _email; }
    }

    public void Logout()
    {
      if (!Connected)
        return;

      FB.LogOut();
      _username = string.Empty;
      _userId = string.Empty;
      _accessToken = string.Empty;
      _photoUrl = string.Empty;
//      dispatcher.Dispatch(AuthenticationProviderEvent.Logout, ServiceType.Facebook);
    }

    public ServiceType Type
    {
      get { return ServiceType.Facebook; }
    }

    public int Priority
    {
      get { return 1; }
    }

    private void ProfilePhotoCallback(IGraphResult result)
    {
      if (String.IsNullOrEmpty(result.Error) && !result.Cancelled)
      {
        IDictionary data = result.ResultDictionary["data"] as IDictionary;
        _photoUrl = data["url"] as String;
      }

//      dispatcher.Dispatch(AuthenticationProviderEvent.LoggedIn, ServiceType.Facebook);
    }
  }
}