﻿using Facebook.Unity;
using UnityEngine;

namespace Scripts.Service.Tracking.Imp
{
    public class FacebookSessionTracker:MonoBehaviour
    {
        
        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    FB.Init(FB.ActivateApp);
                }
            }
        }
    }
}