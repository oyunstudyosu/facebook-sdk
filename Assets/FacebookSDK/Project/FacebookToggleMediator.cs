﻿using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using Scripts.Service.Authentication.Providers;

namespace Scripts.Service.Authentication.View
{
    public enum FacebookToggleEvent
    {
        Click
    }

    public class FacebookToggleMediator : EventMediator
    {
    [Inject]
    public FacebookToggleView view { get; set; }

    [Inject(ServiceType.Facebook)]
    public IAuthenticationProvider authProvider { get; set; }

    private bool _busy;

    public override void OnRegister()
    {
      view.dispatcher.AddListener(FacebookToggleEvent.Click, OnClick);

//      dispatcher.AddListener(AuthenticationProviderEvent.LoggedIn, OnLoggedIn);
//      dispatcher.AddListener(AuthenticationProviderEvent.LoginError, OnLoginError);
//      dispatcher.AddListener(AuthenticationProviderEvent.Logout, OnLogout);

      view.SetKey(authProvider.Connected ? "Logout" : "Login");
      view.LogoutButton.SetActive(authProvider.Connected);
    }

    private void OnLoginError(IEvent payload)
    {
      if (payload.data == null || (ServiceType) payload.data != ServiceType.Facebook)
        return;

//      dispatcher.Dispatch(ScreenEvent.HideLoader);

      view.SetKey("TryAgain");
      _busy = false;
    }

    private void OnLogout(IEvent payload)
    {
      if ((ServiceType) payload.data != ServiceType.Facebook)
        return;

      view.LogoutButton.SetActive(false);
      view.SetKey("Login");
      _busy = false;
    }

    private void OnLoggedIn(IEvent payload)
    {
      if (payload.data == null || (ServiceType) payload.data != ServiceType.Facebook)
        return;

      view.LogoutButton.SetActive(true);
      view.SetKey("Logout");
      _busy = false;
    }

    private void OnClick(IEvent payload)
    {
      if (_busy)
        return;

      _busy = true;

//      dispatcher.Dispatch(ScreenEvent.ShowLoader);

      if (authProvider.Connected)
      {
        authProvider.Logout();
      }
      else
      {
        view.SetKey("Loging in...");
        authProvider.Login();
      }
    }

    public override void OnRemove()
    {
//      dispatcher.RemoveListener(AuthenticationProviderEvent.LoggedIn, OnLoggedIn);
//      dispatcher.RemoveListener(AuthenticationProviderEvent.LoginError, OnLoginError);
//      dispatcher.RemoveListener(AuthenticationProviderEvent.Logout, OnLogout);

      view.dispatcher.RemoveListener(FacebookToggleEvent.Click, OnClick);
    }
    }
}