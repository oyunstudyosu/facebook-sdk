﻿using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Service.Authentication.View
{
  public class FacebookToggleView : EventView
  {
    public Text Label;

    public TextMeshProUGUI TMPLabel;

    public GameObject LogoutButton;

    public void OnButtonClick()
    {
      dispatcher.Dispatch(FacebookToggleEvent.Click);
    }

    public void SetKey(string key)
    {
//            GetComponentInChildren<Translate>().SetKey(key);
    }
  }
}