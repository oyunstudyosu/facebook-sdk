﻿//using System.Collections.Generic;
//using Facebook.Unity;
//using strange.extensions.context.api;
//using UnityEngine;
//
//namespace Scripts.Service.Tracking.Imp
//{
//    public class FacebookTrackingService : ITrackingService
//    {
//        [Inject(ContextKeys.CONTEXT_VIEW)]
//        public GameObject contextView { get; set; }
//
//        [PostConstruct]
//        public void OnPostConstruct()
//        {
//            if (FB.IsInitialized)
//            {
//                FB.ActivateApp();
//            }
//            else
//            {
//                FB.Init(FB.ActivateApp);
//            }
//
//            contextView.AddComponent<FacebookSessionTracker>();
//        }
//
//        public void Earn(string source, string currency, float price)
//        {
//            FB.LogAppEvent("Earn", price, new Dictionary<string, object>
//            {
//                { "Source",source},
//                { "Currency",currency},
//                { "Price",price}
//            });
//        }
//
//        public void Purchase(string item, string currency, float price)
//        {
//            FB.LogPurchase(price, currency, new Dictionary<string, object>
//            {
//                {"Item", item}
//            });
//        }
//
//        public void Transaction(string product, string currency, float price, string receipt, string signature)
//        {
//            FB.LogAppEvent("Transaction", price, new Dictionary<string, object>
//            {
//                { "Product",product},
//                { "Currency",currency},
//                { "Price",price},
//                { "Receipt",receipt},
//                { "Signature",signature}
//            });
//        }
//
//        public void Screen(string name)
//        {
//            FB.LogAppEvent("ViewedContent", null, new Dictionary<string, object>
//            {
//                { "Name",name}
//            });
//        }
//
//        public void Event(string category, string action, string label, int value)
//        {
//            FB.LogAppEvent(category, null, new Dictionary<string, object>
//            {
//                { "Action",action},
//                { "Label",label},
//                { "Value",value}
//            });
//        }
//
//        public void Event(string category, string action, string label)
//        {
//            FB.LogAppEvent(category, null, new Dictionary<string, object>
//            {
//                { "Action",action}
//            });
//        }
//
//        public void Event(string category, string action)
//        {
//            FB.LogAppEvent(category, null, new Dictionary<string, object>
//            {
//                { "Action",action}
//            });
//        }
//
//        public void Error(string name)
//        {
//            FB.LogAppEvent("Error", null, new Dictionary<string, object>
//            {
//                { "Name",name}
//            });
//        }
//
//        public void Event(string category, string action, string label, string value)
//        {
//            FB.LogAppEvent(category, null, new Dictionary<string, object>
//            {
//                { "Action",action},
//                { "Label",label},
//                { "Value",value}
//            });
//        }
//
//        public void Heatmap(string category, Vector3 position)
//        {
//
//        }
//    }
//}